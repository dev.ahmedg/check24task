package com.check24.task.repo

import android.content.Context
import com.check24.base.utils.NetworkHelper
import com.check24.data.keyValue.KeyValueStore
import com.check24.data.network.Resource
import com.check24.data.network.entities.ProductsOverviewDto
import com.check24.data.repository.products.ProductsRemoteDataSource
import com.check24.data.repository.products.ProductsRepo
import com.check24.data.repository.products.ProductsRepoImpl
import com.check24.task.R
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class ProductsRepoImplTest{

    private lateinit var productsRepo: ProductsRepo

    @Mock
    private lateinit var remoteDataSource: ProductsRemoteDataSource
    @Mock
    private lateinit var context: Context
    @Mock
    private lateinit var keyValueStore: KeyValueStore
    @Mock
    private lateinit var  networkHelper: NetworkHelper



    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        productsRepo = ProductsRepoImpl(context, networkHelper, keyValueStore,remoteDataSource)
    }


    @Test
    fun `test GetProductsOverView When Network is Connected`(): Unit = runBlocking {

        val result = Mockito.mock(ProductsOverviewDto::class.java)

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)

        Mockito.`when`(remoteDataSource.getProductsOverview())
            .thenReturn(result)

        val allEmits = productsRepo.getProductsOverviewFlow().toList()

        Assert.assertEquals(2,allEmits.size)

        allEmits[0] shouldBeInstanceOf Resource.Loading::class.java
        allEmits[1] shouldBeInstanceOf Resource.Success::class.java

        (allEmits[1] as Resource.Success).data shouldBeEqualTo result

        Mockito.verify(remoteDataSource, Mockito.times(1))
            .getProductsOverview()

    }


    @Test
    fun `test GetProductsOverView When Network is not Connected`(): Unit = runBlocking {

        val error = "No internet connection"

        Mockito.`when`(context.getString(R.string.no_internet_connection))
            .thenReturn(error)

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(false)


        val allEmits = productsRepo.getProductsOverviewFlow().toList()

        Assert.assertEquals(allEmits.size,2)

        allEmits[0] shouldBeInstanceOf Resource.Loading::class.java
        allEmits[1] shouldBeInstanceOf Resource.Error::class.java
        ( allEmits[1] as Resource.Error ).exception.message shouldBeEqualTo error

        Mockito.verify(remoteDataSource, Mockito.never())
            .getProductsOverview()

    }


}