package com.check24.productlist.view

import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.check24.base.baseEntities.BaseActivity
import com.check24.productdetails.navigation.ProductDetailsNavigation
import com.check24.productlist.R
import com.check24.productlist.databinding.ActivityProductOverviewBinding
import com.check24.productlist.viewModel.ProductOverviewUiModel
import com.check24.productlist.viewModel.ProductsItemUiModel
import com.check24.productlist.viewModel.ProductsOverviewUiState
import com.check24.productlist.viewModel.ProductsOverviewViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class ProductsOverviewActivity : BaseActivity<ActivityProductOverviewBinding>() {


    private val mViewModel by inject<ProductsOverviewViewModel>()

    private val mAdapter = ProductsAdapter{
        showDetails(it)
    }

    private fun showDetails(it: ProductsItemUiModel) {
        ProductDetailsNavigation().startDetailsScreen(it.dto)
    }

    override fun getLayoutRes(): Int  = R.layout.activity_product_overview

    override fun getToolbarTitle(): Any? = null

    override fun onViewAttach() {
        super.onViewAttach()
        setUpRv()
        observeData()
    }

    private fun observeData() {
        lifecycleScope.launch {
            mViewModel.uiState
                .flowWithLifecycle(lifecycle)
                .collect {
                    when(it){
                        is ProductsOverviewUiState.Loading -> showLoading(mBinding.stateful)
                        is ProductsOverviewUiState.Error -> showError(mBinding.stateful,it.errorMsg) { mViewModel.fetchData() }
                        is ProductsOverviewUiState.Success -> {
                            showContent(mBinding.stateful)
                            bindData(it.data)
                        }
                    }
                }
        }
    }

    private fun bindData(data: ProductOverviewUiModel) {
        mAdapter.submitList(data.products)
    }

    private fun setUpRv() {
        mBinding.rv.adapter = mAdapter
    }
}