package com.check24.productlist.navigation

import com.check24.base.baseEntities.BaseNavigationManager
import com.check24.productlist.view.ProductsOverviewActivity

class ProductsOverViewNavigation : BaseNavigationManager() {

    fun startOverViewScreen(){
        start(ProductsOverviewActivity::class.java)
    }



}