package com.check24.productlist.di


import com.check24.productlist.domian.GetProductsOverviewUseCase
import com.check24.productlist.viewModel.ProductsOverviewViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val productsOverviewModule = module {

    factory { GetProductsOverviewUseCase(get()) }

    viewModel {ProductsOverviewViewModel(androidApplication(),get())}
}