package com.check24.productlist.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.check24.data.network.Resource
import com.check24.data.network.entities.ProductsOverviewDto
import com.check24.productlist.R
import com.check24.productlist.domian.GetProductsOverviewUseCase
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class ProductsOverviewViewModel(private val context: Application,private val productsOverviewUseCase: GetProductsOverviewUseCase):ViewModel(){


    private val _uiState = MutableStateFlow<ProductsOverviewUiState>(ProductsOverviewUiState.Loading)
    val uiState  = _uiState.asStateFlow()


    init {
        fetchData()
    }


    fun fetchData() {
        viewModelScope.launch {
            productsOverviewUseCase()
                .map {
                    when(it){
                        is Resource.Success ->{
                            ProductsOverviewUiState.Success(toProductsOverviewUiModel(it.data!!))
                        }
                        is Resource.Loading -> ProductsOverviewUiState.Loading
                        is Resource.Error -> ProductsOverviewUiState.Error(it.exception.message?:context.getString(R.string.something_went_wrong))
                    }
                }
                .collect {
                    _uiState.value = it
                }
        }
    }


    private fun toProductsOverviewUiModel(dto: ProductsOverviewDto):ProductOverviewUiModel{

        return ProductOverviewUiModel(
            headerDescription = dto.header.headerDescription,
            headerTitle = dto.header.headerTitle,
            filters = dto.filters,
            products = dto.products.map {

                val spf = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())



                val releaseDate = spf.format(Date(TimeUnit.SECONDS.toMillis(it.releaseDate)))

                val price = "${it.price.value} ${it.price.currency}"
                ProductsItemUiModel(
                    releaseDate = releaseDate,
                    priceTxt = price,
                    imageURL = it.imageURL,
                    name = it.name,
                    available = it.available,
                    rating = it.rating,
                    description = it.description,
                    id = it.id,
                    dto = it
                )
            }
        )
    }



}