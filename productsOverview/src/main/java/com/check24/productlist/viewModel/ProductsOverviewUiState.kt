package com.check24.productlist.viewModel


sealed class ProductsOverviewUiState {
    data class Success(val data: ProductOverviewUiModel) : ProductsOverviewUiState()
    data class Error(val errorMsg: String) : ProductsOverviewUiState()
    object Loading : ProductsOverviewUiState()
}