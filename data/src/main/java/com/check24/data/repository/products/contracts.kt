package com.check24.data.repository.products

import com.check24.data.network.entities.ProductsOverviewDto
import com.check24.data.network.Resource
import kotlinx.coroutines.flow.Flow


interface ProductsRemoteDataSource {

    suspend fun getProductsOverview(): ProductsOverviewDto

}


interface ProductsRepo {

    fun getProductsOverviewFlow(): Flow<Resource<ProductsOverviewDto>>

    fun toggleProductFav(id:Int,fav: Boolean)

    fun isProductFav(id: Int) : Flow<Resource<Boolean>>

}