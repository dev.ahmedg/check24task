package com.check24.base.utils

import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.check24.base.R


@BindingAdapter("app:image")
    fun bindImage(view: ImageView, url: String?) {
    val color = ColorDrawable(ContextCompat.getColor(view.context, R.color.white_gray))
    Glide.with(view.context)
        .load(url)
        .placeholder(color)
        .error(color)
        .into(view)
    }